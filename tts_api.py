from __future__ import annotations
import json
import random
from copy import deepcopy
from typing import List
from scryfall_api.scryfall import Card as ScryfallCard
from secrets import token_hex

class SavedObject:

    def __init__(self,
                 SaveName: str = "",
                 Date: str = "",
                 VersionNumber: str = "",
                 GameMode: str = "",
                 GameType: str = "",
                 GameComplexity: str = "",
                 Tags: list = [],
                 Gravity: float = 0.5,
                 PlayArea: float = 0.5,
                 Table: str = "",
                 Sky: str = "",
                 Note: str = "",
                 TabStates: dict = {},
                 LuaScript: str = "",
                 LuaScriptState: str = "",
                 XmlUI: str = "",
                 ObjectStates: list = [],
                 ):
        self.SaveName = SaveName
        self.Date = Date
        self.VersionNumber = VersionNumber
        self.GameMode = GameMode
        self.GameType = GameType
        self.GameComplexity = GameComplexity
        self.Tags = Tags
        self.Gravity = Gravity
        self.PlayArea = PlayArea
        self.Table = Table
        self.Sky = Sky
        self.Note = Note
        self.TabStates = TabStates
        self.LuaScript = LuaScript
        self.LuaScriptState = LuaScriptState 
        self.XmlUI = XmlUI
        self.ObjectStates = ObjectStates

    def __repr__(self):
        return json.dumps(self.__dict__, indent=4)


class GameObject:


    def __init__(self, Name: str):
        self.GUID = self.gen_hex_GUID()
        self.Name = Name
        self.Transform = {
            "posX": 2.67508984,
            "posY": 0.97360456,
            "posZ": -5.495264,
            "rotX": 2.31962645E-06,
            "rotY": 0.001997597,
            "rotZ": 180.0,
            "scaleX": 1.0,
            "scaleY": 1.0,
            "scaleZ": 1.0
        }
        self.ColorDiffuse = {
            "r": 0.713235259,
            "g": 0.713235259,
            "b": 0.713235259
        }

    def to_saved_object(self) -> SavedObject:
        return SavedObject(SaveName=self.Name, ObjectStates=[self.__dict__])

    def save(self, filepath) -> None:
        with open(filepath, "r") as file:
            file.write(self.to_saved_object().__repr__())

    def __repr__(self) -> str:
        return json.dumps(self.__dict__, indent=4)

    @staticmethod
    def gen_hex_GUID() -> str:
        return token_hex(2)


class Card(GameObject):

    def __init__(self,
                 Nickname: str,
                 Description: str,
                 FaceURL: str,
                 BackURL: str,
                 card_id: int = None):
        super().__init__(Name="Card")
        self.Nickname = Nickname
        self.Description = Description
        if not card_id:
            card_id = self.generate_ID()
        self.CustomDeck = self.CustomDeckCard(card_id, FaceURL, BackURL).__dict__()
        self.LayoutGroupSortIndex= 0
        self.Value= 0
        self.Locked= False
        self.Grid= True
        self.Snap= True
        self.IgnoreFoW = False
        self.MeasureMovement = False
        self.DragSelectable = True
        self.Autoraise = True
        self.Sticky = True
        self.Tooltip = True
        self.GridProjection = False
        self.HideWhenFaceDown = True
        self.Hands = True
        self.CardID = card_id * 100
        self.SidewaysCard = False
        self.LuaScript = ""
        self.LuaScriptState = ""
        self.XmlUi = ""

    def add_state(self, card: Card, order: int = 2) -> None:
        setattr(self, "States", {order: card.__dict__})

    @staticmethod
    def generate_ID():
        return random.randint(5000, 9999)

    def get_card_id(self):
        return self.CardID/100

    @classmethod
    def generate_custom_card_from_scryfall(cls, card: ScryfallCard, amount: int = 1) -> List[Card]:
        """
        Generate a list of Cards for a given ScryfallCard and its amount
        """
        card_id_front = cls.generate_ID()
        card_id_back = cls.generate_ID()
        cards = []
        for _ in range(amount):
            tts_card = cls(Nickname=card.name,
                           Description=getattr(card, "oracle_text", ""),
                           FaceURL=card.get_main_image_url(),
                           BackURL=card.generic_back_url,
                           card_id=card_id_front)
            if card.is_double_sided():
                # Double-sided card
                side = card.card_faces[1]
                tts_side = cls(Nickname=side["name"],
                               Description=side["oracle_text"],
                               FaceURL=card.get_images_url()[1],
                               BackURL=card.generic_back_url,
                               card_id=card_id_back)
                tts_card.add_state(tts_side)
            cards.append(tts_card)
        return cards

    class CustomDeckCard:
        """
        TTS seems to treat card as a single card deck
        """

        def __init__(self,
                     CardID: int,
                     FaceURL: str,
                     BackURL: str,
                     NumWidth: int = 1,
                     NumHeight: int = 1,
                     BackIsHidden: bool = True,
                     UniqueBack: bool = False,
                     Type: int = 0):
            self.CardID = CardID
            self.FaceURL = FaceURL
            self.BackURL = BackURL
            self.NumWidth = NumWidth
            self.NumHeight = NumHeight
            self.BackIsHidden = BackIsHidden
            self.UniqueBack = UniqueBack
            self.Type = Type
            
        def __repr__(self):
            return {self.CardID: {
                "FaceURL": self.FaceURL,
                "BackURL": self.BackURL,
                "NumWidth": self.NumWidth,
                "NumHeight": self.NumHeight,
                "BackIsHidden": self.BackIsHidden,
                "UniqueBack": self.UniqueBack,
                "Type": self.Type
            }}

        def __dict__(self):
            return {self.CardID: {
                "FaceURL": self.FaceURL,
                "BackURL": self.BackURL,
                "NumWidth": self.NumWidth,
                "NumHeight": self.NumHeight,
                "BackIsHidden": self.BackIsHidden,
                "UniqueBack": self.UniqueBack,
                "Type": self.Type
            }}


class Deck(GameObject):

    def __init__(self, cards: List[Card], Nickname: str = "", Description: str = ""):
        super().__init__(Name="Deck")
        contained_objects = []
        deck_ids = []
        custom_deck = {}
        for card in cards:
            contained_objects.append(card.__dict__)
            deck_ids.append(card.CardID)
            for key, value in card.CustomDeck.items():
                custom_deck[key] = value
        self.Nickname = Nickname
        self.Description = Description
        self.LayoutGroupSortIndex= 0
        self.Value= 0
        self.Locked= False
        self.Grid= True
        self.Snap= True
        self.IgnoreFoW = False
        self.MeasureMovement = False
        self.DragSelectable = True
        self.Autoraise = True
        self.Sticky = True
        self.Tooltip = True
        self.GridProjection = False
        self.HideWhenFaceDown = True
        self.Hands = False
        self.SidewaysCard = False
        self.DeckIDs = [card.CardID for card in cards]
        self.CustomDeck = custom_deck
        self.LuaScript = ""
        self.LuaScriptState = ""
        self.XmlUi = ""
        self.cards = cards
        self.ContainedObjects = [card.__dict__ for card in cards]

    def to_tts_json(self) -> dict:
        """
        Remove cards attribute for rendering
        """
        result = deepcopy(self.__dict__)
        del result["cards"]
        return result

    def __repr__(self) -> str:
        return json.dumps(self.to_tts_json(), indent=4)

    def to_saved_object(self) -> SavedObject:
        return SavedObject(SaveName=self.Nickname, ObjectStates=[self.to_tts_json()])

    def __len__(self) -> int:
        return len(self.cards)


if __name__ == "__main__":
    c = Card("test_card", "description de ma carte", "https://c1.scryfall.com/file/scryfall-cards/normal/front/2/f/2f053406-d2e4-4443-81f1-025e3b27c65a.jpg",
             "https://gamepedia.cursecdn.com/mtgsalvation_gamepedia/f/f8/Magic_card_back.jpg?version=0ddc8d41c3b69c2c3c4bb5d72669ffd7")
    c1 = Card("test_card", "description de ma carte", "https://c1.scryfall.com/file/scryfall-cards/normal/front/2/f/2f053406-d2e4-4443-81f1-025e3b27c65a.jpg",
             "https://gamepedia.cursecdn.com/mtgsalvation_gamepedia/f/f8/Magic_card_back.jpg?version=0ddc8d41c3b69c2c3c4bb5d72669ffd7")
    d = Deck([c, c1], "test_deck", "desc")
    print(d.to_saved_object())
