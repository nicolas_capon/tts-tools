import re
import os
import tqdm
import json
import argparse
import time
from typing import List
from datetime import datetime
from scryfall_api.scryfall import Scryfall, Card
from deckstats_api.deckstats import Deck, Card as DeckstatsCard
from tts_objects.custom_deck import CustomDeck
from tts_objects.custom_card import CustomCard
from tts_objects.saved_object import SavedObject
from shutil import copy

time1 = time.time()
# Parsing
parser = argparse.ArgumentParser(description="Load deck from list")

parser.add_argument('-i', "--input", help="Deck file as input")
parser.add_argument('-o', "--output", help="File path as output")
parser.add_argument("-t", "--token", action="store_true", help="Also create a related token deck (True by default)")
parser.add_argument("-d", "--description", help="Deck description")
args = parser.parse_args()

input_file = args.input
output_file = args.output
include_token = args.token
description = args.description

num_regex = r"^(\d{0,3})\s"
exp_regex = r"[\[\(]([a-zA-Z0-9]{3,4})[\]\)]"
url_regex = r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"

decklist = []
token_list = []
errors = []
scry = Scryfall()

def generate_custom_card_from_scryfall(card: Card, num: int = 1) -> List[CustomCard]:
    card_id_front = CustomCard.generate_ID()
    card_id_back = CustomCard.generate_ID()
    cards = []
    for _ in range(num):
        tts_card = CustomCard(Nickname=card.name,
                              Description=getattr(card, "oracle_text", ""),
                              FaceURL=card.get_main_image_url(),
                              BackURL=card.generic_back_url,
                              card_id=card_id_front)
        if card.is_double_sided():
            # Double-sided card
            side = card.card_faces[1]
            tts_side = CustomCard(Nickname=side["name"],
                                  Description=side["oracle_text"],
                                  FaceURL=card.get_images_url()[1],
                                  BackURL=card.generic_back_url,
                                  card_id=card_id_back)
            tts_card.add_state(tts_side)
        cards.append(tts_card)
    return cards

# Get the deck list
if re.search(url_regex, input_file):
    # deck is from the web
    deck = Deck.from_url(input_file)
    description = deck.description
    lines = tqdm.tqdm(deck.cards, desc="Generating decklist from scryfall.com")
    for line in lines:
        if card:=scry.get_card(exact=line.name):
            decklist += generate_custom_card_from_scryfall(card, line.amount)
            if include_token:
                token_list += card.get_related_cards(type_filter="token")
        else:
            errors.append(line)

else:
    # from local file
    with open(input_file, "r") as f:
        deck_data = f.read()

    lines = tqdm.tqdm(deck_data.split("\n"), desc="Generating decklist from scryfall.com")
    for line in lines:
        if not line or line.startswith("//"):
            continue
        l = line # copy to modify
        num, exp, url = "", "", ""
        # number of cards
        s = re.findall(num_regex, l)
        if s:
            num = s[0]
            l = re.sub(num_regex, "", l)
        # expansion
        s = re.findall(exp_regex, l)
        if s:
            exp = s[0]
            l = re.sub(exp_regex, "", l)
        # URL
        s = re.findall(url_regex, l)
        if s:
            url = s[0]
            l = re.sub(url_regex, "", l)
        # name
        name = l.strip()
        card_id = CustomCard.generate_ID()
        if url:
            decklist += [CustomCard(Nickname=name,
                                    Description="",
                                    FaceURL=url,
                                    BackURL=Card.generic_back_url,
                                    card_id=card_id)
                         for _ in range(num)]
        else:
            card = scry.get_card(exact=name)
            if card:
                decklist += generate_custom_card_from_scryfall(card, int(num))
                if include_token:
                    token_list += card.get_related_cards(type_filter="token")
            else:
                errors.append(line)

[print(f"Card not found: {e}") for e in errors]
if output_file:
    full_filename, extension = os.path.splitext(output_file)
    filename = os.path.basename(output_file)
else:
    full_filename = ""
    extension = None
    filename = "deck"

current_time = datetime.now().strftime("[%d-%m-%Y %H:%M:%S]")
if not description:
    description = "My deck"
deck = CustomDeck(decklist, filename, f"{current_time} {description}")
saved_deck = SavedObject(ObjectStates=[deck.__dict__])

if include_token and token_list:
    tokens = []
    for t in token_list:
        tokens += generate_custom_card_from_scryfall(t)
    token_deck = CustomDeck(tokens, f"{filename}_tokens", f"{current_time} tokens")
    saved_token_deck = SavedObject(ObjectStates=[token_deck.__dict__])
else:
    token_deck, saved_token_deck = None, None

if output_file:
    with open(output_file, "w") as file:
        json.dump(saved_deck.__dict__, file, indent=2)
    # copy deck image too
    deck_image = os.path.join(os.path.dirname(__file__), "images", "card_back.png")
    copy(deck_image, full_filename + ".png")
    if saved_token_deck:
        with open(f"{full_filename}_tokens{extension}", "w") as file:
            json.dump(saved_token_deck.__dict__, file, indent=2)
        deck_image = os.path.join(os.path.dirname(__file__), "images", "card_back.png")
        copy(deck_image, f"{full_filename}_tokens.png")
    time2 = time.time()
    duration = f"{round((time2 - time1), 3)}"
    print(f"Deck generated [{len(decklist)} cards] in {duration}s")
else:
    # Stdout
    print(json.dumps(saved_deck.__dict__, indent=2))
    if saved_token_deck:
        print(json.dumps(saved_token_deck))
