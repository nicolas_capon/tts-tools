import pytest
from model import Deck

deck_url = "https://deckstats.net/decks/52569/2438492-edh-with-sideboard-and-double-"

@pytest.fixture
def deckstats_deck_url() -> str:
    return deck_url

@pytest.fixture
def deckstats_deck() -> Deck:
    return Deck.from_deckstats(deck_url)

@pytest.fixture
def cubecobra_id() -> str:
    return "myc"
