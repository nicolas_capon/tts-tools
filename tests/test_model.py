import os
import re
import sys
sys.path.append("..")
import pytest
import model

def test_import_deckstats(deckstats_deck_url: str):
    deck = model.Deck.from_deckstats(deckstats_deck_url)
    assert len(deck) == 100 and len(deck.sideboard) == 15

def test_import_deckstats_to_cockatrice(deckstats_deck: model.Deck):
    test_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files", "EDH_with_sideboard_and_double_faced_cards.cod")
    with open(test_file, "r") as file:
        data_test = file.read()
    assert data_test.strip("\n") == deckstats_deck.to_cockatrice().strip("\n")

@pytest.mark.skip
def test_import_deckstats_to_tts(deckstats_deck: model.Deck):
    # TODO: How to test this ?
    test_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files", "EDH_with_sideboard_and_double_faced_cards.json")
    with open(test_file, "r") as file:
        data_test = file.read()
    main, tokens, sideboard = deckstats_deck.to_tts(include_tokens=True, include_sideboard=True)
    # Remove all RNG from data thanks to regex
    main_str = main.__repr__()
    regs = [r'"GUID": "(.+)",|FaceURL": "(.+)",', r'\s+([0-9]{6})', r'"([0-9]{4})"']
    for reg in regs:
        data_test = re.sub(reg, "", data_test)
        main_str = re.sub(reg, "", main_str)

    assert data_test.strip("\n") == main_str.strip("\n")
