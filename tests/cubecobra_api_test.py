import sys
import pytest
sys.path.append("..")
import cubecobra_api

def test_fetch_cards(cubecobra_id: str):
    assert len(cubecobra_api.Cube.fetch_cards(cubecobra_id)) == 360

def test_fetch_infos(cubecobra_id: str):
    assert cubecobra_api.Cube.fetch_infos(cubecobra_id) == ("Multiplayer Yolo Cube", "Multiplayer Yolo Cube")

def test_fetch_updates(cubecobra_id: str):
    assert len(cubecobra_api.Cube.fetch_updates(cubecobra_id)) > 0

