import re
import csv
import requests
import feedparser
import logging
from bs4 import BeautifulSoup
from time import struct_time
from typing import Dict, List, Union, Tuple

CardUpdate = Tuple[Union[None, str], Union[None, str]]

class Card:

    name: str
    cmc: int
    card_type: str
    color: str
    expansion: str
    collector_number: str
    rarity: str
    color_category: str
    status: str
    finish: str
    maybeboard: bool
    image_url: str
    back_image_url: str
    tags: str
    note: str
    mtgo_id: int

    def __init__(self, **cubecobra_card: Dict[str, str]):
        self.name = cubecobra_card.get("Name")
        self.cmc = cubecobra_card.get("CMC")
        if self.cmc:
            try:
                self.cmc = int(cubecobra_card.get("CMC"))
            except ValueError as e:
                logging.error(e)
                self.cmc = None
        self.card_type = cubecobra_card.get("Type")
        self.color = cubecobra_card.get("Color")
        self.expansion = cubecobra_card.get("Set")
        self.collector_number = cubecobra_card.get("Collector Number")
        self.rarity = cubecobra_card.get("Rarity")
        self.color_category = cubecobra_card.get("Color Category")
        self.status = cubecobra_card.get("Status")
        self.finish = cubecobra_card.get("Finish")
        match cubecobra_card.get("Maybeboard"):
            case "true":
                self.maybeboard = True
            case "false":
                self.maybeboard = False
            case _:
                self.maybeboard = False
        self.image_url = cubecobra_card.get("Image URL")
        self.back_image_url = cubecobra_card.get("Image Back URL")
        self.tags = cubecobra_card.get("Tags")
        self.note = cubecobra_card.get("Notes")
        self.mtgo_id = cubecobra_card.get("MTGO ID")
        if self.mtgo_id:
            self.mtgo_id = int(cubecobra_card.get("MTGO ID"))

    def to_csv(self):
        """
        Name,CMC,Type,Color,Set,Collector Number,Rarity,Color Category,Status,Finish,Maybeboard,Image URL,Image Back URL,Tags,Notes,MTGO ID
        """
        return f"\"{self.name}\",{self.cmc},\"{self.card_type}\",{self.color},\"{self.expansion}\",\"{self.collector_number}\",{self.rarity},{self.color_category},{self.status},{self.finish},{str(self.maybeboard).lower()},{self.image_url},{self.back_image_url},\"{self.tags}\",\"{self.note}\",{self.mtgo_id}"

    def __repr__(self) -> str:
        return f"<Card ({self.__dict__})>"


class Update:

    def __init__(self, title: str, description: str, guid: str, publication_date: struct_time):
        self.title = title
        self.description = description
        self.guid = guid
        self.publication_date = publication_date
        self.card_updates = self.parse_card_updates()

    def parse_card_updates(self) -> List[CardUpdate]:
        """
        Parse card update in html description of an Update
        Return list of Tuple(old_card, new_card) for a swap between cards
        If old_card is None then a card is added
        If new_card is None then a card is removed
        """
        card_updates = []
        soup = BeautifulSoup(self.description, features="html.parser", multi_valued_attributes=None)
        if soup.div.get("class") == "change-set":
            # Split card updates by br tag
            for change in re.split(r"<\s*br\s*\/>", self.description):
                change_soup = BeautifulSoup(change, features="html.parser")
                cards = change_soup.find_all("a")
                if change_soup.span and change_soup.span.string == "→" and len(cards) == 2:
                    # Swap cards
                    card_updates.append((Card(Name=cards[0].string), Card(Name=cards[1].string)))
                elif change_soup.span and change_soup.span.string == "+" and len(cards) == 1:
                    # Add card
                    card_updates.append((None , Card(Name=cards[0].string)))
                elif change_soup.span and change_soup.span.string == "-" and len(cards) == 1:
                    # Remove card
                    card_updates.append((Card(Name=cards[0].string), None))
        return card_updates

    def __repr__(self) -> str:
        return f"<Update ({self.__dict__})>"


class Cube:

    def __init__(self, cubecobra_id: str):
        self.id: str = cubecobra_id
        self.name, self.subtitle = self.fetch_infos(cubecobra_id)
        self.cards = self.fetch_cards(cubecobra_id)
        self.updates = self.fetch_updates(cubecobra_id)

    @staticmethod
    def fetch_cards(cubecobra_id: str) -> List[Card]:
        """
        Return list of Cards for a cube specified by its ID
        """
        url = f"https://cubecobra.com/cube/download/csv/{cubecobra_id}"
        response = requests.get(url)
        cards = []
        if response.ok:
            data = response.text
            csv_reader = csv.DictReader(data.splitlines())
            cards += [Card(**c) for c in list(csv_reader)]
        else:
            logging.warning(f"[Error {response.status_code}] Could not fetch cube info from {url}")
        return cards

    @staticmethod
    def fetch_infos(cubecobra_id: str) -> Tuple[str, str]:
        """
        Return name and subtitle for a cube specified by its ID
        """
        url = f"https://cubecobra.com/cube/rss/{cubecobra_id}"
        rss = feedparser.parse(url).feed
        if not rss:
            logging.warning(f"Error fetching cube infos from {url}")
        return rss.get("title"), rss.get("subtitle")

    @staticmethod
    def fetch_updates(cubecobra_id: str) -> List[Update]:
        """
        Return a list of Update for a cube specified by its ID
        """
        url = f"https://cubecobra.com/cube/rss/{cubecobra_id}"
        rss = feedparser.parse(url).entries
        if not rss:
            logging.warning(f"Error fetching cube infos from {url}")
        return [Update(u.get("title"), u.get("description"), u.get("guid"), u.get("published_parsed")) for u in rss]

    def export(self, output: str = "csv") -> Union[str, None]:
        if output == "csv": 
            csv = "Name,CMC,Type,Color,Set,Collector Number,Rarity,Color Category,Status,Finish,Maybeboard,Image URL,Image Back URL,Tags,Notes,MTGO ID\n"
            csv += "\n".join([card.to_csv() for card in self.cards])
            return csv
        elif output == "txt":
            return "\n".join([card.name for card in self.cards])

    def __repr__(self) -> str:
        return f"<Cube (id={self.id}, name={self.name}, cards_len={len(self.cards)}, updates_len={len(self.updates)})>"

