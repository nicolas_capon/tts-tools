from __future__ import annotations
import re
import json
import requests
import datetime
import logging
from typing import Union

class Deck:

    name: str
    description: str
    cards: list[Card]
    sideboard: list[Card]

    def __init__(self, **entries):
            self.__dict__.update(entries)


    @classmethod
    def from_url(cls, url: str) -> Union[Deck, None]:
        """Load deck from target deckstat url
        Return a Deck Object with attributes like name, cards, description, sideboard...
        :param url: Deck URL from deckstat domain
        :return: Deck Object as dict
        """
        p = re.compile(r"deckstats\.net/decks/")
        m = p.search(url)
        if not m: return
        r = requests.get(url)
        content = {}
        if r.ok:
            data = r.content.decode('utf-8')
            p = re.compile(r"init_deck_data\((.*), (true|false)\);deck_display")
            m = p.search(data)
            if not m: return
            content = json.loads(m.group(1))
            cards = []
            for section in content.get("sections", [{}]):
                cards += [Card(**c) for c in section.get("cards", [])]
            content["sideboard"] = [Card(**c) for c in content.get("sideboard", [])]
            del content["sections"]
            content["cards"] = cards
            return Deck(**content)


    def get_deck_url(self) -> Union[str, None]:
        """
        Create an online deck on deckstats.net from a list of Cards and title
        :params deck: Deck Object with name cards attributes
        :return: URL of the given deck or None
        """
        if not self.cards:
            return None
        if not self.name:
            timestamp = datetime.date.today().strftime("%d-%m-%Y")
            self.name = f"My deck [{timestamp}]"
        #Prepare request
        decklist = ""
        for i, card in enumerate(self.cards):
            br = "\n"
            note = " #"
            decklist += f"{card.amount}x {card.name}{note+card.note if card.note else ''}{br if i<len(self.cards)-1 else ''}"
        return get_url(decklist, self.name)


class Card:

    name: str
    amount: int = 1
    note: str
    expansion: str

    def __init__(self, **entries):
        self.__dict__.update(entries)


def get_url(decklist: str, decktitle: str) -> Union[str, None]:
    """
    Create an online deck on deckstats.net from a decklist
    :params decklist: str List of cards in the good format (ex: 1x [expansion] Card Name\n)
    :params decktitle: str Title of the given deck
    :return: str URL of the created online deck or None
    """
    url = 'https://deckstats.net/index.php'
    headers = {"Content-type":"application/x-www-form-urlencoded"}
    data = {"deck": decklist, "decktitle":decktitle.encode('latin-1')}
    # Request and handle error status
    r = requests.post(url, data=data, headers=headers)
    if r.ok:
        # Regex to find deck url in page content
        regex = "<meta property=\"og:url\" content=\"([^\"]+)\""
        m = re.findall(regex, r.text)
        if m:
            return m[0]
        else:
            logging.info(f"Match not found in page content for deck [{decktitle}]")
            return None
    else:
        logging.info("Deckstat request failed {0}.".format(r))
        return None


if __name__ == "__main__":
    deck1 = Deck.from_url("https://deckstats.net/decks/52569/1841513-draft-de-nicolas-du-02-12-2020?share_key=MXAtDmlExsQZU72B")
    deck2 = Deck.from_url("https://deckstats.net/decks/52569/2427348-omnath-locus-of-creation/en")

    print(deck1.name, deck2.name)
