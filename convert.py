import os
import re
import argparse
from model import Deck

parser = argparse.ArgumentParser(
    description="MTG Deck converter [Cockatrice | Tabletop Simulator | CubeCobra | Deckstats]"
)
parser.add_argument("-i", "--input", help="Deck filepath or URL")
parser.add_argument("-o", "--output", help="Directory to save the deck")
parser.add_argument(
    "-f",
    "--format",
    help="Deck format as output [cockatrice, tts, cubecobra, deckstats]",
)
parser.add_argument(
    "-t", "--tokens", help="Add tokens (separate deck file with _tokens extension)"
)
parser.add_argument(
    "-s",
    "--sideboard",
    help="Add sideboard (separate deck file with _sideboard extension for TTS)",
)
parser.add_argument("-n", "--name", help="Deck name")
parser.add_argument("-d", "--description", help="Deck description")
args = parser.parse_args()

url_regex = (
    r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
)

deck = None
if re.search(url_regex, args.input):
    deck = Deck().from_deckstats(args.input)
elif os.path.splitext(args.input) == ".json":
    deck = Deck().from_tts(args.input)
elif os.path.splitext(args.input) == ".cod":
    deck = Deck().from_cockatrice(args.input)
elif args.input.isalnum():
    deck = Deck().from_cubecobra(args.input)

if deck:
    data, f = "", ".txt"
    match args.format:
        case "cockatrice":
            data = deck.to_cockatrice()
            f = ".cod"
        case "cubecobra":
            data = deck.to_cubecobra()
        case "deckstats":
            data = deck.to_deckstats()
        case "tts":
            data = deck.to_tts()
            f = ".json"
        case _:
            pass

    if args.output:
        if type(data) == tuple:
            # data is a tuple of TTSDeck
            for d in data:
                with open(os.path.join(args.output, d.SaveName + f), "w") as file:
                    file.write(d.__repr__())
        else:
            with open(os.path.join(args.output, deck.name + f), "w") as file:
                file.write(data)
    else:
        # output is stdout
        print(data)
