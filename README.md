# Tabletop Simulator Object Generator

## Installation

1. Download and Install [Python](https://www.python.org/downloads/)
2. Open terminal and verify python installation
```
python -V && pip -V
```
3. Clone this projet then go into it
```
git clone --recursive https://gitlab.com/nicolas_capon/tts_tools.git
cd tts_tools
```
4. Create virtual environment (optional)
```
python -m venv venv
source venv/bin/activate
```
5. Install dependencies
```
pip install -r requirements.txt
```

You are ready to go.

## Quick look

To display help
```
python convert.py -h
```

Syntax:
```
python convert.py --input [Cube ID | Deck URL | Deck Filepath] --output [directory path] --format [cockatrice | tts | deckstats | cubecobra] 
```

Example: To generate a deck from cubecobra to cockatrice format
```
python convert.py -i [cube_id] -f cockatrice
```

## Developpement

### Automated Testing

download pytest
```
pip install pytest
```

Run the tests
```
python -m pytest tests/
```

## TODO

- [ ] Finish to_cubecobra method
- [ ] Add .from_txt and .to_txt to model.Deck and convert.py
- [ ] Remove deck_parser.py and finish convert.py (tokens and sideboard args)
- [ ] Add default deck image for tts and possibility to choose one
- [ ] Add default saved folder to saved objects
