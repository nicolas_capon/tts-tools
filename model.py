from __future__ import annotations
import re
import json
import requests
import datetime
import tqdm
from typing import Union, List, Tuple
from lxml import etree
from collections import Counter
import deckstats_api as deckstats
import cubecobra_api as cubecobra
from tts_api import Card as TTSCard, Deck as TTSDeck
import scryfall_api.scryfall as scryfall_api

class Deck:

    name: str
    description: str
    main: list[Card]
    sideboard: list[Card]

    def __init__(self, **kwargs):
        [setattr(self, k, v) for k, v in kwargs.items()]
        if not getattr(self, "description", None):
            self.description = ""
        if not getattr(self, "main", None):
            self.main = []
        if not getattr(self, "sideboard", None):
            self.sideboard = []

    @classmethod
    def from_cockatrice(cls, filepath: str) -> Deck:
        with open(filepath, "r", encoding="utf8") as f:
            xml = f.read()
        root = etree.fromstring(bytes(xml, encoding='utf8'))
        name, description, main, sideboard = "", "", [], []
        for appt in root.getchildren():
            match appt.tag:
                case "deckname":
                    name = appt.text
                case "comments":
                    description = appt.text
                case "zone":
                    if appt.attrib["name"] == "main":
                        main = [Card(name=card.attrib["name"], amount=int(card.attrib["number"])) for card in appt.getchildren()]
                    if appt.attrib["name"] == "sideboard":
                        sideboard = [Card(name=card.attrib["name"], amount=int(card.attrib["number"])) for card in appt.getchildren()]
                case _:
                    pass
        return cls(name=name, description=description, main=main, sideboard=sideboard)

    def to_cockatrice(self) -> str:
        """
        Return this deck in cockatrice format (xml)
        add .cod extension if you want to save this deck
        """
        xml = etree.Element("cockatrice_deck")
        xml.set("version", "1")
        deckname = etree.SubElement(xml, "deckname")
        deckname.text = self.name
        description = etree.SubElement(xml, "comments")
        description.text = getattr(self, "description", "")
        main = etree.SubElement(xml, "zone")
        main.set("name", "main")
        for card in self.main:
            c = etree.SubElement(main, "card")
            c.set("number", str(card.amount))
            c.set("name", str(card.name))
        if len(self.sideboard):
            sideboard = etree.SubElement(xml, "zone")
            sideboard.set("name", "side")
            for card in self.sideboard:
                c = etree.SubElement(sideboard, "card")
                c.set("number", str(card.amount))
                c.set("name", str(card.name))
        tree = etree.ElementTree(xml)
        etree.indent(xml, space="    ")
        return etree.tostring(tree, xml_declaration=True, pretty_print=True, encoding="UTF-8").decode("utf-8")

    @classmethod
    def from_deckstats(cls, url: str) -> Union[Deck, None]:
        """Load deck from target deckstat url
        Return a Deck Object with attributes like name, cards, description, sideboard...
        :param url: Deck URL from deckstat domain
        :return: Deck Object as dict
        """
        p = re.compile(r"deckstats\.net/decks/")
        m = p.search(url)
        if not m: return
        r = requests.get(url)
        content = {}
        if r.ok:
            data = r.content.decode('utf-8')
            p = re.compile(r"init_deck_data\((.*), (true|false)\);deck_display")
            m = p.search(data)
            if not m: return
            content = json.loads(m.group(1))
            cards = []
            for section in content.get("sections", [{}]):
                cards += [Card(**c) for c in section.get("cards", [])]
            content["sideboard"] = [Card(**c) for c in content.get("sideboard", [])]
            del content["sections"]
            content["main"] = cards
            return cls(**content)

    def to_deckstats(self) -> Union[str, None]:
        """
        Create an online deck on deckstats.net from a list of Cards and title
        :params deck: Deck Object with name cards attributes
        :return: URL of the given deck or None
        """
        if not self.main:
            return None
        if not self.name:
            timestamp = datetime.date.today().strftime("%d-%m-%Y")
            self.name = f"My deck [{timestamp}]"
        #Prepare request
        decklist = ""
        br = "\n"
        note = " #"
        for i, card in enumerate(self.main):
            decklist += f"{card.amount}x {card.name}{note+getattr(card, 'note', None) if getattr(card, 'note', None) else ''}{br if (i<len(self.main)-1) else ''}"
        for card in getattr(self, "sideboard", []):
            decklist += f"\nSB: {card.amount}x {card.name}{note+getattr(card, 'note', None) if getattr(card, 'note', None) else ''}"
        return deckstats.get_url(decklist, self.name)

    @classmethod
    def from_tts(cls, filepath: str) -> Deck:
        """
        Create Deck Object from Tabletop Simulator json deck
        :params filepath: Path of the JSON deck file
        :return: Deck
        """
        with open(filepath, "r", encoding="utf8") as file:
            data = json.loads(file.read())
        name = data.get("ObjectStates")[0].get("Nickname")
        description = data.get("ObjectStates")[0].get("Description")
        main = [Card(name=cardname, amount=amount) for cardname, amount in Counter(c.get("Nickname") for c in data.get("ObjectStates")[0].get("ContainedObjects")).items()]
        return cls(name=name, description=description, main=main)

    def to_tts(self,include_sideboard=True, include_tokens=True) -> tuple:
        """
        Return this deck in Tabletop simulator format (json)
        :returns: str ready to be saved
        """
        main, sideboard, tokens = [], [], []
        tts_side = None
        for card in tqdm.tqdm(self.main, desc="Import main deck from scryfall.com"):
            cards, tokens = card.to_tts(include_tokens=include_tokens)
            main += cards
            tokens += tokens
        tts_main = TTSDeck(cards=main, Nickname=self.name, Description=self.description).to_saved_object()
        if include_sideboard:
            for card in tqdm.tqdm(self.sideboard, desc="Import sideboard from scryfall.com"):
                cards, tokens = card.to_tts(include_tokens=include_tokens)
                sideboard += cards
                tokens += tokens
            tts_side = TTSDeck(cards=sideboard,
                               Nickname=self.name+"_sideboard",
                               Description="[SIDEBOARD] " + self.description).to_saved_object()
        tts_tokens = TTSDeck(cards=tokens,
                             Nickname=self.name+"_tokens",
                             Description="[TOKENS] " + self.description).to_saved_object()
        # Remove duplicates in tokens
        tokens = set(tokens)
        return tts_main, tts_tokens, tts_side

    @classmethod
    def from_cubecobra(cls, cubecobra_id: str) -> Deck:
        cube = cubecobra.Cube(cubecobra_id)
        deck = cls(name=cube.name, description=cube.subtitle)
        for card in cube.cards:
            c = Card(**card.__dict__)
            if c.maybeboard:
                deck.sideboard.append(c)
            else:
                deck.main.append(c)
        return deck

    def to_cubecobra(self) -> str:
        """
        return CSV file in cubecobra ready to be import as new cube
        """
        csv = "Name,CMC,Type,Color,Set,Collector Number,Rarity,Color Category,Status,Finish,Maybeboard,Image URL,Image Back URL,Tags,Notes,MTGO ID\n"
        csv += "\n".join([card.to_csv() for card in self.main])



    def __len__(self) -> int:
        cpt = 0
        for card in self.main:
            cpt += card.amount
        return cpt

    def __repr__(self) -> str:
        return f"<Deck(name={self.name}, description={self.description}, main_len={len(self.main)}, sideboard_len={len(self.sideboard)})"


class Card:

    name: str
    amount: int = 1
    note: str
    expansion: str
    image_url: str
    text: str
    back_image_url: str

    def __init__(self, name: str, **kwargs):
        self.name = name
        for attribute, value in kwargs.items():
            if not getattr(self, "amount", None):
                self.amount = 1
            elif not getattr(self, "back_image_url", None):
                self.back_image_url = scryfall_api.Card.generic_back_url
            else:
                setattr(self, attribute, value)

    def to_tts(self, include_tokens: bool = True) -> Tuple[List[TTSCard], List[TTSCard]]:
        cards, tokens = [], []
        if url:=getattr(self, "image_url", None):
            cards += [TTSCard(Nickname=self.name,
                                 Description=getattr(self, "text", ""),
                                 FaceURL=url,
                                 BackURL=self.back_image_url,
                                 card_id=TTSCard.generate_ID())
                         for _ in range(self.amount)]
        else:
            if scryfall_card:=scryfall_api.Scryfall().get_card(exact=self.name):
                cards += TTSCard.generate_custom_card_from_scryfall(scryfall_card, amount=self.amount)
                if include_tokens:
                    for sc in scryfall_card.get_related_cards(type_filter="token"):
                        tokens += TTSCard.generate_custom_card_from_scryfall(sc)
                    # tokens += [TTSCard.generate_custom_card_from_scryfall(sc) for sc in scryfall_card.get_related_cards(type_filter="token")]
        return cards, tokens

    def to_cubecobra(self, maybeboard: bool = False) -> cubecobra.Card:
        # TODO convert custom cards
        if scryfall_card:=scryfall_api.Scryfall().get_card(exact=self.name):
            match len(scryfall_card.colors):
                case 0:
                    color_category = "c"
                case 1:
                    color_category = scryfall_card.colors[0].lower()
                case _:
                    color_category = "m"
            if "Land" in scryfall_card.type_line:
                color_category = "l"
            return cubecobra.Card({"Name":self.name,
                                   "CMC":scryfall_card.cmc,
                                   "Type":scryfall_card.type_line,
                                   "Color":"".join(scryfall_card.colors),
                                   "Set":scryfall_card.set.lower(),
                                   "Collector Number":str(scryfall_card.collector_number),
                                   "Rarity":scryfall_card.rarity,
                                   "Color Category":color_category,
                                   "Status":"Owned",
                                   "Finish":"Non-foil",
                                   "Maybeboard": str(maybeboard).lower(),
                                   "Image URL": "",
                                   "Image Back URL": "",
                                   "Tags": "",
                                   "Notes": ""})

    @classmethod
    def from_scryfall_card(cls, card: scryfall_api.Card, amount: int = 1):
        setattr(card, "amount", amount)
        a = {}
        for attribute in dir(card):
            if not "__" in attribute:
                a[attribute] = getattr(card, attribute)
        return cls(**a)


if __name__ == "__main__":
    deck = Deck.from_cubecobra("7zv")
    print(deck)
